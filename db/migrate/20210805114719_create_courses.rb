class CreateCourses < ActiveRecord::Migration[6.1]
  def change
    create_table :courses do |t|
      t.string :title
      t.string :subject
      t.string :description
      t.integer :duration_in_weeks

      t.timestamps
    end
  end
end
