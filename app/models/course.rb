class Course < ApplicationRecord
  has_many :tutors

  validates :title, :subject, :duration_in_weeks, presence: true
  validates :title, uniqueness: true

  def create_tutors(tutors_params)
    self.tutors.create(tutors_params)
  end
end
