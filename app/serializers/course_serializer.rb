class CourseSerializer < ActiveModel::Serializer
  attributes :id, :title, :subject, :description, :duration_in_weeks, :tutors

  def tutors
     ActiveModelSerializers::SerializableResource.new(object.tutors, each_serializer: TutorSerializer)
  end
end
