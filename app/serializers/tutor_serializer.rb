class TutorSerializer < ActiveModel::Serializer
  attribute :id, if: -> { object.errors.blank? }
  attribute :name, if: -> { object.errors.blank? }
  attribute :email, if: -> { object.errors.blank? }
  attribute :errors, if: -> { object.errors.present? }

  def errors
    object.errors.messages
  end
end
