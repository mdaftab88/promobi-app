class CoursesController < ApplicationController
  def index
    @courses = Course.includes(:tutors)

    render json: @courses, include: :tutors
  end

  def create
    @course = Course.new(course_params)
    Course.transaction do
      if @course.save
        @course.create_tutors(tutors_params) if tutors_params.present?
        render json: @course, include: :tutors
      else
        render json: @course.errors, status: :unprocessable_entity
      end
    rescue StandardError => ex
      render json: {message: ex.message}
    end
  end

  private
  def permitted_params
    @permitted_params ||= params.require(:course).permit(:title, :subject, :description, :duration_in_weeks, tutors: [:name, :email])
  end

  def course_params
    permitted_params.except(:tutors)
  end

  def tutors_params
    permitted_params[:tutors]
  end
end
