FactoryBot.define do
  factory :course do
    title { Faker::Educator.course_name }
    subject { Faker::Educator.subject }
    description { Faker::Lorem.sentence }
    duration_in_weeks { Faker::Number.between(from: 4, to: 24)  }
  end
end
