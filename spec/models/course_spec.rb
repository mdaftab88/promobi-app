require 'rails_helper'

RSpec.describe Course, type: :model do
  it "is valid with title, subject, description and duration" do
    course = FactoryBot.build(:course)
    expect(course).to be_valid
  end

  describe "When title is blank" do
    course = FactoryBot.build(:course, title: nil)

    it "is invalid" do
      expect(course).to_not be_valid
    end

    it "should give an error message" do
      course.valid?
      expect(course.errors[:title]).to include("can't be blank")
    end
  end

  describe "When subject is blank" do
    course = FactoryBot.build(:course, subject: nil)

    it "is invalid" do
      expect(course).to_not be_valid
    end

    it "should give an error message" do
      course.valid?
      expect(course.errors[:subject]).to include("can't be blank")
    end
  end

  describe "When duration is blank" do
    course = FactoryBot.build(:course, duration_in_weeks: nil)

    it "is invalid" do
      expect(course).to_not be_valid
    end

    it "should give an error message" do
      course.valid?
      expect(course.errors[:duration_in_weeks]).to include("can't be blank")
    end
  end

  describe "When title is duplicate" do
    @title = Faker::Educator.course_name

    course1 = FactoryBot.create(:course, title: @title)
    course = FactoryBot.build(:course, title: @title)

    it "is invalid" do
      expect(course).to_not be_valid
    end

    it "should give an error message" do
      course.valid?
      expect(course.errors[:title]).to include("has already been taken")
    end
  end
end
