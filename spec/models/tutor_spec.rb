require 'rails_helper'

RSpec.describe Tutor, type: :model do
  it "is valid with email and name" do
    tutor = FactoryBot.build(:tutor)
    expect(tutor).to be_valid
  end

  describe "When email is blank" do
    tutor = FactoryBot.build(:tutor, email: nil)

    it "is invalid" do
      expect(tutor).to_not be_valid
    end

    it "should give an error message" do
      tutor.valid?
      expect(tutor.errors[:email]).to include("can't be blank")
    end
  end

  describe "When email is not unique" do
    @email = Faker::Internet.email

    tutor1 = FactoryBot.create(:tutor, email: @email)
    tutor = FactoryBot.build(:tutor, email: @email)

    it "is invalid" do
      expect(tutor).to_not be_valid
    end

    it "should give an error message" do
      tutor.valid?
      expect(tutor.errors[:email]).to include("has already been taken")
    end
  end
end
