

***POST api to create `course` and its `tutors`***
```
curl --location --request POST 'localhost:3000/courses' \
--header 'Content-Type: application/json' \
--data-raw '{
    "course": {
        "title": "Software Engineering Principle",
        "subject": "Software Engineering",
        "description": "Loerm Ipsum",
        "duration_in_weeks": 4,
        "tutors": [
            {
                "name": "Aftab Alam",
                "email": "mdatab88@gmail.com"
            },
            {
                "name": "Alam"
            }
        ]
    }
}'
```


\
***GET api to list all courses and its tutors***
```
curl --location --request GET 'localhost:3000/courses'
```