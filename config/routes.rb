Rails.application.routes.draw do
  get 'courses' => "courses#index"
  post 'courses' => "courses#create"
end
